@testpocapi
Feature: [API AUTHENTICATION] Validar a geracao de token authentication
  Eu como usuario do sistema
  Desejo gerar um token de acesso para o EC

  Scenario Outline: [POST] - Validar a consulta de Token
    Given que realizo a autenticacao utilizando <EC> <usuario> e <senha>
    Then eu recebo o token

    Examples: 
      | EC           | usuario      | senha    |
      | "1014231342" | "1014231342" | "123456" |
