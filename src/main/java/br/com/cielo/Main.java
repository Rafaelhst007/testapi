package br.com.cielo;

import io.cucumber.core.logging.Logger;
import io.cucumber.core.logging.LoggerFactory;

public class Main {

//	public static void main(String[] args) {
//		System.out.println("Hello World");
		
	public static void main(String[] argv) {
        byte exitStatus = run(argv, Thread.currentThread().getContextClassLoader());
        System.exit(exitStatus);
    }

    /**
     * Launches the Cucumber-JVM command line.
     *
     * @param  argv        runtime options. See details in the
     *                     {@code io.cucumber.core.options.Usage.txt} resource.
     * @param  classLoader classloader used to load the runtime
     * @return             0 if execution was successful, 1 if it was not (test
     *                     failures)
     */
    public static byte run(String[] argv, ClassLoader classLoader) {        
	        return io.cucumber.core.cli.Main.run(argv, classLoader);
	}

//  }

}
