package br.com.cielo.rest.config;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class StepData {

	public Response response;
	public RequestSpecification request;
	public String token;
}
