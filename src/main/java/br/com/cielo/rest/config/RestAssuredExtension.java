package br.com.cielo.rest.config;

import java.util.Map;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class RestAssuredExtension {
	private static  String uri = "https://digitaldev.hdevelo.com.br" ;
	public static RequestSpecification Request;
	
	public RestAssuredExtension() {
		// TODO Auto-generated constructor stub
		RequestSpecBuilder builder = new RequestSpecBuilder();
		builder.setBaseUri(uri);
		builder.setContentType(ContentType.JSON);
		builder.addHeader("Client-id", "SITE");
		RequestSpecification requestSpec = builder.build();
		Request = RestAssured.given().spec(requestSpec);
	}
	
	// POST COM BODY E SEM TOKEN
	public static Response PostOps(String url, Map<String, Object> body) {
	Request.body(body);
	return Request.post(url);
	}
	
	public static Response GetOps(String url, String token) { 
		Request.headers("authorization", "Bearer "+ token);
		return Request.get(url);
	}
	
}
