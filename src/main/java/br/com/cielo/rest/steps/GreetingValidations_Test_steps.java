package br.com.cielo.rest.steps;

import java.util.HashMap;
import java.util.Map;

import org.hamcrest.Matchers;

import br.com.cielo.rest.config.RestAssuredExtension;
import br.com.cielo.rest.config.StepData;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;

//import static org.hamcrest.Matchers.containsString;
//import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;



public class GreetingValidations_Test_steps {
	
	private static final String urlApi = "http://localhost:8080";
	private StepData stepData;
	
	public GreetingValidations_Test_steps(StepData stepData) {
		this.stepData = stepData;		
	}
	
	@Given("que a API esta em funcionamento")
	public void que_a_API_esta_em_funcionamento () {
		System.out.println("======= Validar teste da API Grreting e suas respostas ======");
		System.out.println("Dado");		
	}	
	
	@When("efetuar chamadas com novos cadastros")
	public void efetuar_chamadas_com_novos_cadastros () {
		System.out.println("Quando");
		stepData.response = RestAssuredExtension.GetOps(urlApi + "/greeting?name=Poc_Api", null);
	}

	@Then("devera ser exibido conforme cada cadastro")
	public void devera_ser_exibido_conforme_cada_cadastro() {
		System.out.println("Ent�o");
		
		Integer statusCode = stepData.response.statusCode();
		assertThat(statusCode, is(200));
		
		Integer idGreet = stepData.response.getBody().jsonPath().get("id");
		assertThat(idGreet, greaterThan(0));	
		
		String content = stepData.response.getBody().jsonPath().get("content");		
		assertThat(content, containsString("Poc"));		
	}
}
