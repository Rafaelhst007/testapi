package br.com.cielo.rest.steps;


import static org.hamcrest.Matchers.is;

import br.com.cielo.rest.config.RestAssuredExtension;
import br.com.cielo.rest.config.StepData;

import static org.hamcrest.MatcherAssert.assertThat;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class DashboardMonth_steps {
	private static final String urlPath = "/farol/backoffice/billing/dashboard" ;
	private StepData stepData;
	
	public DashboardMonth_steps(StepData stepData) {  //ctrl + espa�o
		// TODO Auto-generated constructor stub
		this.stepData = stepData;	
		
	}
	
	@When("requisitar")
	public void requisitar() {
	    // Write code here that turns the phrase above into concrete actions
	 stepData.response = RestAssuredExtension.GetOps(urlPath + "/monthly", stepData.token);   
	}

	@Then("exibira o dash")
	public void exibira_o_dash() {
	    // Write code here that turns the phrase above into concrete actions
			System.out.println(stepData.response.getBody().prettyPrint());
		
		String qtdDivida = stepData.response.getBody().jsonPath().get("quantityEcsBilling");
			System.out.println(qtdDivida);
		
		String monthYear = stepData.response.getBody().jsonPath().get("listInfo[0].monthYear");
			System.out.println(monthYear);
			
		assertThat(qtdDivida, is("303.0"));
	}
}
