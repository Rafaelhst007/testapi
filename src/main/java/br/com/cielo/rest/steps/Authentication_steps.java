package br.com.cielo.rest.steps;

import java.util.HashMap;
import java.util.Map;

import br.com.cielo.rest.config.RestAssuredExtension;
import br.com.cielo.rest.config.StepData;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.RestAssured;

public class Authentication_steps {
	
	private static final String urlPath = "/authentication";
	private StepData stepData;
	
	public Authentication_steps(StepData stepData) {
		this.stepData = stepData;		
	}
	
	@Given("que realizo a autenticacao utilizando {string} {string} e {string}")
	public void que_realizo_a_autenticacao_utilizando_e(String ec, String usuario, String senha) {
		Map<String, Object> body = new HashMap<String, Object>(); 
		body.put("merchant", ec);
		body.put("username", usuario);
		body.put("password", senha);
		
		stepData.response = RestAssuredExtension.PostOps(urlPath, body);	
		stepData.token = stepData.response.jsonPath().getString("accessToken.token");	
	}	

	@Then("eu recebo o token")
	public void eu_recebo_o_token() {
//		stepData.response.prettyPrint();		
		System.out.println(stepData.token);
	
	}

}
