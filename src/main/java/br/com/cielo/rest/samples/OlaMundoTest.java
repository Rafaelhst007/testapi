package br.com.cielo.rest.samples;

import static io.restassured.RestAssured.*;// com * n�o precisamos colocar o "RestAssured" antes do metodo

import org.junit.Assert;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;

public class OlaMundoTest {

	@Test
	public void testOlaMundo() {
		Response response = request(Method.GET, "https://restapi.wcaquino.me/ola");
//		System.out.println(response.getBody().asString().equals("Ola Mundo!"));
//		System.out.println(response.statusCode() == 200);
		Assert.assertTrue(response.getBody().asString().equals("Ola Mundo!"));
		Assert.assertTrue("Passed", response.statusCode() == 200);
		Assert.assertTrue("Fail", response.statusCode() == 201);
				
		ValidatableResponse validacao = response.then();
		validacao.statusCode(200);
	}

	@Test
	public void outroTesteRestAssured() {
//		Response response = RestAssured.request(Method.GET, "https://restapi.wcaquino.me/ola");
//		ValidatableResponse validacao = response.then();
//		validacao.statusCode(200);
		
//		RestAssured.get("https://restapi.wcaquino.me/ola").then().statusCode(200);//substitui todo o c�digo acima
		get("https://restapi.wcaquino.me/ola").then().statusCode(200);//substitui todo o c�digo acima
		
//=============Given - When - Then
		
		//given().when().get("https://restapi.wcaquino.me/ola").then().statusCode(200); //ou...
		
		given() //Dado (Pr� condi��o)			
		.when()	//Quando (A��o)
			.get("https://restapi.wcaquino.me/ola")
		.then() //Ent�o (Resultado esperado)
			.statusCode(200);
	}	
	
}
