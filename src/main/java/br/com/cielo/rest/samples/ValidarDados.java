package br.com.cielo.rest.samples;

import static io.restassured.RestAssured.*;// com * n�o precisamos colocar o "RestAssured" antes do metodo
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;

public class ValidarDados {
	
	@Test
	public void validarMatchesHamcrest() {
	
		Assert.assertThat("Maria", Matchers.is("Maria"));
		Assert.assertThat(128, Matchers.is(128));//igual
		Assert.assertThat(128, Matchers.isA(Integer.class));
		Assert.assertThat(128d, Matchers.isA(Double.class));
		Assert.assertThat(128d, Matchers.greaterThan(126d));//maior que
		Assert.assertThat(128d, Matchers.lessThan(130d)); //menor que
		
		//Listas
		
		List<Integer> impares = Arrays.asList(1,3,5,7,9);
		Assert.assertThat(impares, Matchers.hasSize(5));//tamanho da lista
		assertThat(impares, hasSize(5));//tamanho da lista
		assertThat(impares, contains(1,3,5,7,9));//contem exatamente
		assertThat(impares, containsInAnyOrder(1,3,5,9,7));
		assertThat(impares, hasItem(1));//checa um item
		assertThat(impares, hasItems(1,7));//checa mais de um item
		
		assertThat("Maria", is(not("Jo�o")));//Diferente de 
		assertThat("Maria", (not("Jo�o")));  //Diferente de 
		assertThat("Joaquina", anyOf(is("Maria"), is("Joaquina")));//compara
		assertThat("Joaquina", allOf(startsWith("Joa"), endsWith("ina"), containsString("qui")));//valida peda�os
		
		
	}
	

}
