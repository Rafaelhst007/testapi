package br.com.cielo.rest.samples;

import static io.restassured.RestAssured.*;// com * n�o precisamos colocar o "RestAssured" antes do metodo
import static org.hamcrest.Matchers.*; //importa todos os matches
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;

public class ValidarBody {
	
/*	@Test
	public void validarBody01() {
		
		given() 			
		.when()	
			.get("https://restapi.wcaquino.me/ola")
		.then() 
			.statusCode(200)
			.body(is("Ola Mundo!"))
			.body(containsString("Mundo"))
			.body(is(not(nullValue())));
	}
*/
	
	@Test
	public void validarPrimeiroNivel() {
		
		given() 			
		.when()	
			.get("https://restapi.wcaquino.me/users/1")
		.then() 
			.statusCode(200)  // valida status
			.body("id", is(1)) // Valida Id			
			.body("name", containsString("Silva")) //Valida parte do nome)
			.body("age", greaterThan(18)); // Valida se � maior de 18 anos
	}
	
	@Test
	public void  validarBodyOutrasFormas() {
		Response response = RestAssured.request(Method.GET,"https://restapi.wcaquino.me/users/1");
		
		//path		
		//Assert.assertEquals(1, response.path("id"));
		//Assert.assertEquals(1, response.path("%s","id"));
		
		//jsonpath
		JsonPath jpath = new JsonPath(response.asString());
		Assert.assertEquals(1, jpath.getInt("id"));
		
		//From 
		int id = JsonPath.from(response.asString()).getInt("id");
		Assert.assertEquals(1, id);
	}
	
	@Test
	public void validarjsonsegundonivel() {
		given() 			
		.when()	
			.get("https://restapi.wcaquino.me/users/2")
		.then() 
			.statusCode(200)  // valida status
			.body("id", is(2)) // Valida Id			
			.body("name", containsString("Joaquina")) //Valida parte do nome)
			.body("endereco.rua", containsString("Rua dos bobos")) 	
			.body("endereco.numero", is(0));
		
	}
	
	@Test
	public void validarListaJson() {
		given() 			
		.when()	
			.get("https://restapi.wcaquino.me/users/3")
		.then() 
			.statusCode(200)  // valida status
			.body("id", is(3)) // Valida Id			
			.body("name", containsString("Ana")) //Valida parte do nome)
			
			.body("filhos", hasSize(2)) 	
			.body("filhos[0].name", is("Zezinho"))
			.body("filhos[1].name", is("Luizinho"))
			.body("filhos.name", hasItem("Zezinho"))
			.body("filhos.name", hasItems("Zezinho","Luizinho"))
			;
		
	}
	
	@Test
	public void validarUsuarioInexistente() {
		given() 			
		.when()	
			.get("https://restapi.wcaquino.me/users/4")
		.then() 
			.statusCode(404)						
			.body("error", is("Usu�rio inexistente"))
			;
		
	}

	@Test
	public void validarListaRaiz() {
		given() 			
		.when()	
			.get("https://restapi.wcaquino.me/users")
		.then() 
			.statusCode(200)						
			.body("$", hasSize(3))
			.body("name", hasItems("Jo�o da Silva", "Maria Joaquina","Ana J�lia"))
			.body("age[1]", is(25))
			.body("filhos.name", hasItem(Arrays.asList("Zezinho","Luizinho")))
			.body("salary", contains(1234.5678f, 2500, null))
			;
		
	}
}
