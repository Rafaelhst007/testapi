package br.com.cielo.rest.runner;

import org.junit.runner.RunWith;

//import frameworkUtils.ReportingUtils;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;


@RunWith(Cucumber.class)
@CucumberOptions(
		features = "features", 
		plugin = {"html:target/cucumber-report.html","json:target/cucumber.json" },
		glue = { "br.com.cielo.rest.steps" }, 
		tags = "@testpocapi",		
		monochrome = true)
		
public class TestRunner {
	
}
